# Objects

# Bugs / Ants
 * Can be selected in groups with left mouse button.
 * Can be commanded to move with the right mouse button.
 * Can collect food. When moved to a spot close to a food source it will
   pick that up and carry it to an ant nest. After dropping it to the ant
   nest they will move back towards the food source. If the ant dies or gets new
   commands before the food was brought to the nest, it will be dropped to the
   ground at the ants position.
 * Can attack enemies. When another ant gets into range of a not moving ant
   it will automatically attack that other one and will inform close ants
   about it so groups attack together.
 * May dead bugs / ants drop food so dead enemies can be turned into new bugs
   and the player can restock after he defeated an opponents attack.

# The nest
 * Can be targetted by bugs / ants carrying food.
 * Can produce new bugs / ants when provided with food.

# Food source
 * Can be moved to nests by bugs / ants.
