# Ludum Dare 38: A Small World

## Theme interpretations

 * Only as a small world for the gameplay.
 * The world of ants and bugs (everything that is small from our standpoint).
 * We all live so close together on this small world, damaging other is
   damaging for us as well.
 * An experiment where sience created a small world with living "people".
 * A childs world.
 * A small world in a snow globe. (Wifes hint)
 * The world on a USB stick.
 * Novel books as they carry a compressed, small version of a big world.
 * Think factorio on a really small circular world.
 * Godlike simulator where you control people on a small world and have to help
   them survive.

## Option A: A world of ants and bugs
 * RTS game where ants have to battle bugs (or other ants)?
 * Top down
 * 3 or 4 color palette
 * Problem: AI for the opponent and graphics...

## Option B: Damaging others is damaging yourself #1
 * Defcon like gameplay
 * Every attack has enviromental impact on your own country

## Option C: Damaging others is damaging yourself #2
 * Platformer / Roguelike
 * Weapon shoots radioactive or biolical stuff
 * If the radiation burden gets to high, you die!
 * Problem: Getting physics right.

## Option D: Novel book
 * Point and click adventure?
 * Every page is a screen in the game?
 * Problem: I can not draw :D

## Option E: Circular "Factorio"
 * Rendering in the way No DCMA's Sky does
 * Problem: Would need some kind of tech tree and a lot of balancing.

## Option F: Godlike sim
 * Maybe also rendering in the way No DCMA's Sky does, so the world can be tile
   based.
 * Problem: Would need quite some graphics and goals would need to be defined.
 * Lemmings?

# I guess I'm going with option A.
