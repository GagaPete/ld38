extends Sprite

export(NodePath) var navigation_path
export(int) var team = 1
export(int) var initial_bugs = 5

onready var timer = get_node("timer")
onready var bug_scene = preload("res://objects/bug.tscn")

var food_stock = 0



#
# API
#
func add_to_stock(collectible):
    if collectible.is_in_group("food"):
        food_stock = food_stock + 1
        timer.start()



func spawn_bug():
    var bug = bug_scene.instance()
    bug.navigation_path = navigation_path
    bug.team = team
    get_parent().add_child(bug)
    var offset = Vector2(1, 0)
    randomize()
    offset = offset * randf() * 8
    offset = offset.rotated(randf() * PI * 2)
    bug.set_global_pos(get_global_pos() + offset)
    bug.destination = bug.get_global_pos()



#
# GODOT CALLBACKS
#
func _ready():
    add_to_group("nest_team%d" % team)
    for i in range(0, initial_bugs):
        call_deferred("spawn_bug")



#
# TIMER EVENT HANDLERS
#
func _on_timer_timeout():
    if food_stock > 0:
        spawn_bug()
        food_stock = food_stock - 1
    else:
        timer.stop()
