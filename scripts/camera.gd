#
# 2D Camera controller rightclick and drag to
#
extends Camera2D

onready var level = get_parent()

var velocity = Vector2()
var drag = false
var drag_pos = null



#
# GODOT CALLBACKS
#
func _ready():
    set_fixed_process(true)
    set_process_unhandled_input(true)



func _fixed_process(delta):
    var pos = get_global_pos()
    if drag:
        pos = pos + drag_pos - get_global_mouse_pos()
    else:
        if Input.is_action_pressed("ui_left"):
            velocity.x = velocity.x - 20 * delta
        if Input.is_action_pressed("ui_right"):
            velocity.x = velocity.x + 20 * delta
        if not Input.is_action_pressed("ui_left") and not Input.is_action_pressed("ui_right"):
            velocity.x = 0

        if Input.is_action_pressed("ui_up"):
            velocity.y = velocity.y - 20 * delta
        if Input.is_action_pressed("ui_down"):
            velocity.y = velocity.y + 20 * delta
        if not Input.is_action_pressed("ui_left") and not Input.is_action_pressed("ui_right"):
            velocity.y = 0

    pos = pos + velocity
    pos.x = clamp(pos.x, 0, max(0, level.width - get_viewport().get_visible_rect().end.x))
    pos.y = clamp(pos.y, 0, max(0, level.height - get_viewport().get_visible_rect().end.y))
    set_global_pos(pos)



func _unhandled_input(event):
    if event.type == InputEvent.MOUSE_BUTTON and event.button_index == BUTTON_RIGHT:
        drag = event.pressed
        drag_pos = get_global_mouse_pos()
