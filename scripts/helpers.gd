extends Node


func get_closest_node(global_pos, nodes):
    if nodes.size() == 0:
        return null

    var closest_node = nodes[0]
    var closest_distance = global_pos.distance_squared_to(nodes[0].get_global_pos())
    for node in nodes:
        var node_distance = global_pos.distance_squared_to(node.get_global_pos())
        if node_distance < closest_distance:
            closest_node = node
            closest_distance = node_distance
    return closest_node



var EDGE_OFFSETS = [{"x":0, "y":-1}, {"x":1, "y":-1}, {"x":1, "y":0}, {"x":1, "y":1}, {"x":0, "y":1}, {"x":-1, "y":0}]
func spread_spawn_points(global_pos, radius, count):
    var x = 0
    var y = 0
    var fromEdge = 0 # Von welcher Kante?
    var cells = {}
    var points = []

    if count > 0:
        points.push_back(global_pos)
        cells["0_0"] = true
        count = count - 1

    while count > 0:
        for i in range(0, 6):
            var offset = EDGE_OFFSETS[(i + fromEdge) % 6]
            var offset_x = x + offset.x
            var offset_y = y + offset.y
            if offset.y != 0 && abs(y % 2) == 1:
                offset_x = offset_x - 1
            var cell_key = "%d_%d" % [offset_x, offset_y]
            if not cells.has(cell_key):
                points.push_back(global_pos + Vector2((offset_x - abs(offset_y % 2) * 0.5) * radius * 2, offset_y * radius * 1.7))
                cells[cell_key] = true
                x = offset_x
                y = offset_y
                fromEdge = (i + fromEdge + 3) % 6
                count = count - 1
                break
    return points
