extends Node2D

# This property defines which bugs are controlled
export (int) var team = 1

# Send a list of bugs to the given position
func send_bugs(global_pos, bugs):
    var collectibles = get_tree().get_nodes_in_group("collectible")
    if collectibles.size() > 0:
        for bug in bugs:
            if collectibles.size() > 0:
                var collectible = helpers.get_closest_node(global_pos, collectibles)
                collectibles.erase(collectible) # We don't want to process the same collectible twice
                if collectible.get_global_pos().distance_to(global_pos) < 10:
                    bug.walk_to(collectible.get_global_pos())
                    bugs.erase(bug)
    var positions = helpers.spread_spawn_points(global_pos, 4, bugs.size())
    for bug in bugs:
        bug.walk_to(positions.back())
        positions.pop_back()

# Send a group of five bugs to the given position
# ignore_active: If true, only inactive bugs are sent
func send_group(global_pos, ignore_active=false):
    var bugs = get_own_bugs(ignore_active)
    var closest_bugs = []
    for i in range(0, min(5, bugs.size())):
        var closest_bug = bugs[0]
        var closest_distance = bugs[0].get_global_pos().distance_squared_to(global_pos)
        for bug in bugs:
            if bug.get_global_pos().distance_squared_to(global_pos) < closest_distance:
                closest_bug = bug
                closest_distance = bug.get_global_pos().distance_squared_to(global_pos)
        closest_bugs.push_back(closest_bug)
        bugs.erase(closest_bug)
    send_bugs(global_pos, closest_bugs)

# Send all bugs to the given position
# ignore_active: If true, only inactive bugs are sent
func send_all(global_pos, ignore_active=false):
    send_bugs(global_pos, get_own_bugs(ignore_active))

# Return all the bugs in the players team
# ignore_active: If true, only inactive bugs are returned
func get_own_bugs(ignore_active=false):
    var bugs = get_tree().get_nodes_in_group("bug_team%d" % team)
    if ignore_active:
        for bug in bugs:
            if bug.active:
                bugs.erase(bug)
    return bugs

# Return all the bugs in the players team
# ignore_active: If true, only inactive bugs are returned
func get_own_nests():
    return get_tree().get_nodes_in_group("nest_team%d" % team)

# Returns a list of enemies bugs
func get_enemies_bugs():
    var bugs = get_tree().get_nodes_in_group("bug")
    for bug in bugs:
        if bug.team == team:
            bugs.erase(bug)
    return bugs