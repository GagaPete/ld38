extends Sprite

onready var title = get_node("container/vbox/title")
onready var camera = get_node("camera")



#
# LEVEL EVENT HANDLERS
#
func _on_level_ended(winner):
    if winner == 0:
        title.set_text("A strange game. The only winning move is not to play.")
    elif winner == 1:
        title.set_text("You've won. Congratulations!")
    elif winner == 2:
        title.set_text("You've lost. Try harder next time.")
    get_tree().set_pause(true)
    camera.make_current()
    show()



#
# BUTTON EVENT HANDLERS
#
func _on_quit_pressed():
    get_tree().set_pause(false)
    get_tree().change_scene("res://menus/main.tscn")
