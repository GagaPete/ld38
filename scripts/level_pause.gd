extends Sprite

onready var pause_camera = get_node("camera")
onready var game_camera = get_node("../camera")



#
# GODOT CALLBACKS
#
func _ready():
    set_process_input(true)



func _input(event):
    if event.is_action_pressed("ui_cancel"):
        if get_tree().is_paused():
            get_tree().set_pause(false)
            game_camera.make_current()
            hide()
        else:
            get_tree().set_pause(true)
            pause_camera.make_current()
            show()



#
# BUTTON HANDLERS
#
func _on_continue_pressed():
    get_tree().set_pause(false)
    game_camera.make_current()
    hide()



func _on_quit_pressed():
    get_tree().set_pause(false)
    get_tree().change_scene("res://menus/main.tscn")
