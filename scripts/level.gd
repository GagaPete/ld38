tool
extends Node2D

signal ended(winner)

export(int, 1, 4096) var width = 1 setget set_width
export(int, 1, 4096) var height = 1 setget set_height



#
# SETTER / GETTER
#
func set_width(new_width):
    width = new_width
    update_shapes()
    update()

func set_height(new_height):
    height = new_height
    update_shapes()
    update()



#
# GODOT CALLBACKS
#
func _ready():
    set_process(true)
    call_deferred("update_shapes")
    call_deferred("update")



func update_shapes():
    if get_tree() == null or get_tree().is_editor_hint():
        return

    var shape = null
    var xcenter = width / 2
    var ycenter = height / 2
    var top_shape = get_node("top/shape")
    shape = top_shape.get_shape()
    shape.set_extents(Vector2(width + 60, 30) / 2)
    top_shape.set_global_pos(Vector2(xcenter - 15, -15))
    top_shape.set_shape(shape)

    var left_shape = get_node("left/shape")
    shape = left_shape.get_shape()
    shape.set_extents(Vector2(30, height + 60) / 2)
    left_shape.set_global_pos(Vector2(-15, ycenter - 15))
    left_shape.set_shape(shape)

    var right_shape = get_node("right/shape")
    shape = right_shape.get_shape()
    shape.set_extents(Vector2(30, height + 60) / 2)
    right_shape.set_global_pos(Vector2(width + 15, ycenter - 15))
    right_shape.set_shape(shape)

    var bottom_shape = get_node("bottom/shape")
    shape = bottom_shape.get_shape()
    shape.set_extents(Vector2(width + 60, 30) / 2)
    bottom_shape.set_global_pos(Vector2(xcenter - 15, height + 15))
    bottom_shape.set_shape(shape)



func _draw():
    if get_tree() == null or not get_tree().is_editor_hint():
        return

    var selection = Rect2(0, 0, width, height)
    var from_x = selection.pos.x + 0.5
    var from_y = selection.pos.y + 0.5
    var to_x = selection.end.x + 0.5
    var to_y = selection.end.y + 0.5
    draw_line(Vector2(from_x, from_y), Vector2(from_x, to_y), Color(1.0, 0.0, 0.5), 1)
    draw_line(Vector2(from_x, to_y), Vector2(to_x, to_y), Color(1.0, 0.0, 0.5), 1)
    draw_line(Vector2(to_x, to_y), Vector2(to_x, from_y), Color(1.0, 0.0, 0.5), 1)
    draw_line(Vector2(to_x, from_y), Vector2(from_x, from_y), Color(1.0, 0.0, 0.5), 1)
    #draw_rect(selection, Color(1.0, 1.0, 1.0, 0.1))



func _process(delta):
    if get_tree() == null or get_tree().is_editor_hint():
        return

    var team1_bug_count = 0
    var team2_bug_count = 0
    for bug in get_tree().get_nodes_in_group("bug"):
        if bug.team == 1:
            team1_bug_count = team1_bug_count + 1
        else:
            team2_bug_count = team2_bug_count + 1

    if team1_bug_count == 0:
        emit_signal("ended", 2)
    elif team2_bug_count == 0:
        emit_signal("ended", 1)
    elif team1_bug_count == 0 and team2_bug_count == 0:
        emit_signal("ended", 0)
