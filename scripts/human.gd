extends "res://scripts/player.gd"

#
# GODOT CALLBACKS
#
func _ready():
    set_process_input(true)

func _input(event):
    var destination_pos = get_global_mouse_pos()
    var ignore_active = not Input.is_action_pressed("game_send_active")
	
    if event.is_action_pressed("game_send_group"):
        send_group(destination_pos, ignore_active)
    elif event.is_action_pressed("game_send_all"):
        send_all(destination_pos, ignore_active)
