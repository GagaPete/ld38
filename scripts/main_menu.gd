extends Control



onready var demo = get_node("demo")
onready var help = get_node("help")



func _ready():
    get_node("menu/fullscreen").set_pressed(OS.is_window_fullscreen())
    if OS.get_name() == "HTML5":
        get_node("menu/quit").queue_free()



func _on_play_slalom_pressed():
    get_tree().change_scene("res://levels/slalom.tscn")



func _on_play_galore_pressed():
    get_tree().change_scene("res://levels/galore.tscn")



func _on_play_crossfire_pressed():
    get_tree().change_scene("res://levels/crossfire.tscn")



func _on_help_toggled(pressed):
    demo.set_hidden(pressed)
    help.set_hidden(!pressed)



func _on_quit_pressed():
    get_tree().quit()



func _on_fullscreen_toggled(pressed):
    OS.set_window_fullscreen(pressed)
