extends KinematicBody2D



# Variable settings
export(NodePath) var navigation_path
export(int) var team = 1

# Constant settings
const SPEED = 80
const ATTACK_MAX_DIST = 18
const PICKUP_MAX_DIST = 8
const DELIVER_MAX_DIST = 8
const FLOCKING_DISTANCE = 6

# Other nodes
onready var navigation = get_node(navigation_path)
onready var cooldown_timer = get_node("cooldown_timer")
onready var sprite = get_node("sprite")

# Skins
var bug_1_skin = preload("res://images/bug_1.png")
var bug_2_skin = preload("res://images/bug_2.png")

# Knowledge
var destination = Vector2()
var reached_destination = true
var velocity = Vector2()
var carry_item = null
var active = false
var health = 100

# Other entities within area
var nearby_bugs = []
var nearby_buddies = []
var nearby_enemies = []
var nearby_collectibles = []



#
# API
#
func walk_to(global_pos):
    drop_carry_item()
    destination = global_pos
    reached_destination = false



func hurt(damage):
    health = health - damage
    if health <= 0:
        drop_carry_item()
        queue_free()



#
# HELPERS
#
func drop_carry_item():
    if carry_item == null:
        return
    remove_child(carry_item)
    get_parent().add_child(carry_item)
    carry_item.set_global_pos(get_global_pos())
    carry_item.add_to_group("collectible")
    carry_item = null



func move_towards(global_pos):
    var path = navigation.get_simple_path(get_global_pos(), global_pos)
    if path.size() > 1:
        velocity = (path[1] - get_global_pos()).normalized()
    else:
        velocity = (global_pos - get_global_pos()).normalized()



func apply_flocking(velocity):
    # based on https://gamedevelopment.tutsplus.com/tutorials/3-simple-rules-of-flocking-behaviors-alignment-cohesion-and-separation--gamedev-3444
    var alignment = Vector2()
    var cohesion = Vector2()
    var separation = Vector2()
    var count = 0
    for bug in nearby_bugs:
        if get_global_pos().distance_to(bug.get_global_pos()) < FLOCKING_DISTANCE:
            alignment = alignment + bug.velocity
            cohesion = cohesion + bug.get_global_pos()
            separation = separation + bug.get_global_pos() - get_global_pos()
            count = count + 1
    if count == 0:
        return velocity
    alignment = (alignment / count).normalized()
    cohesion = ((cohesion / count) - get_global_pos()).normalized()
    separation = (separation * -1).normalized()

    return (velocity * 5 + alignment + cohesion + separation).normalized()



#
# BUG ACTIONS
#
func action_list():
    if action_fight():
        return true
    if action_move_to_new_destination():
        return true
    if action_deliver_food():
        return true
    if action_pickup_food():
        return true
    return false



func action_move_to_new_destination():
    if not reached_destination and get_global_pos().distance_to(destination) > 1:
        move_towards(destination)
        return true
    else:
        reached_destination = true
    return false



func action_deliver_food():
    if carry_item:
        var nest = get_tree().get_nodes_in_group("nest_team%d" % [team])
        if get_global_pos().distance_to(nest[0].get_global_pos()) > DELIVER_MAX_DIST:
            move_towards(nest[0].get_global_pos())
            return true
        else:
            nest[0].add_to_stock(carry_item)
            remove_child(carry_item)
            carry_item = null
            reached_destination = false
    return false



func action_fight():
    for enemy in nearby_enemies:
        if destination.distance_to(enemy.get_global_pos()) < 15:
            enemy.hurt(10 + nearby_buddies.size())
            return true
    return false



func action_pickup_food():
    if carry_item:
        return false

    if nearby_collectibles.size() > 0:
        for collectible in nearby_collectibles:
            if get_global_pos().distance_to(collectible.get_global_pos()) <= PICKUP_MAX_DIST:
                collectible.remove_from_group("collectible")
                collectible.get_parent().remove_child(collectible)
                add_child(collectible)
                collectible.set_pos(Vector2(0, -3))
                carry_item = collectible
                return true
    return false



#
# GODOT CALLBACKS
#
func _ready():
    add_to_group("bug_team%d" % team)
    set_fixed_process(true)
    if team == 1:
        sprite.set_texture(bug_1_skin)
    else:
        sprite.set_texture(bug_2_skin)
    destination = get_global_pos()



func _fixed_process(delta):
    velocity = Vector2()
    active = action_list()

    var movement = apply_flocking(velocity) * SPEED * delta
    if movement.length() > 0:
        sprite.set_global_rot(movement.angle() + PI)
        var rest = move(movement)
        if is_colliding():
            move(get_collision_normal().slide(rest))



#
# AREA EVENT HANDLERS
#
func _on_area_body_enter(body):
    if body == self:
        return #...
    if body.is_in_group("bug"):
        nearby_bugs.append(body)
        if body.team == team:
            nearby_buddies.append(body)
        else:
            nearby_enemies.append(body)
    if body.is_in_group("collectible"):
        nearby_collectibles.append(body)



func _on_area_body_exit(body):
    nearby_bugs.erase(body)
    nearby_buddies.erase(body)
    nearby_enemies.erase(body)
    nearby_collectibles.erase(body)
