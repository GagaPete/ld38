extends "res://scripts/player.gd"

onready var cooldown_timer = get_node("cooldown_timer")

#
# COOLDOWN TIMER EVENT HANDLERS
#
func _on_cooldown_timer_timeout():
    var collectibles = get_tree().get_nodes_in_group("collectible")
    if collectibles.size() != 0:
        # Collect
        if get_own_bugs(true).size() >= 2:
            var nests = get_own_nests()
            var collectible = helpers.get_closest_node(nests[randi()%nests.size()].get_global_pos(), collectibles)
            send_group(collectible.get_global_pos(), true)
    else:
        # Attack
        var bugs = get_enemies_bugs()
        if bugs.size() != 0:
            send_group(bugs[randi()%bugs.size()].get_global_pos(), true)
    cooldown_timer.set_wait_time(0.25 + randf() * 0.75)
