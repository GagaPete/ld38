extends Node2D

onready var team1_nest = get_node("team1_nest")
onready var team1_food = get_node("team1_food")
onready var team2_nest = get_node("team2_nest")
onready var team2_food = get_node("team2_food")

var food_scene = preload("res://objects/medium_patch.tscn")



#
# HELPERS
#
func random_pos():
    return
#
# GODOT CALLBACKS
func _ready():
    set_process(true)
    randomize()
    var position
    var offset

    var phi = randf() * PI * 2
    position = Vector2(250, 180) + Vector2(120, 0).rotated(phi)
    team1_nest.set_pos(position)

    offset = Vector2(50, 0)
    offset = offset.rotated(randf() * PI * 2)
    team1_food.set_pos(position + offset)

    position = Vector2(250, 180) + Vector2(120, 0).rotated(phi + PI)
    team2_nest.set_pos(position)
    offset = Vector2(50, 0)
    offset = offset.rotated(randf() * PI * 2)
    team2_food.set_pos(position + offset)



func _process(delta):
    randomize()
    var team1_bug_count = team1_nest.food_stock
    var team2_bug_count = team2_nest.food_stock
    for bug in get_tree().get_nodes_in_group("bug"):
        if bug.team == 1:
            team1_bug_count = team1_bug_count + 1
        else:
            team2_bug_count = team2_bug_count + 1

    if team1_bug_count == 0 or team2_bug_count == 0:
        var pos_team1 = team1_food.get_global_pos()
        team1_food.queue_free()
        team1_food = food_scene.instance()
        add_child(team1_food)
        team1_food.set_global_pos(pos_team1)

        var pos_team2 = team2_food.get_global_pos()
        team2_food.queue_free()
        team2_food = food_scene.instance()
        add_child(team2_food)
        team2_food.set_global_pos(pos_team2)


        # Spawn new bugs
        var spawn_max = max(5, max(team1_bug_count, team2_bug_count))
        for i in range(team1_bug_count, spawn_max):
            team1_nest.spawn_bug()

        for i in range(team2_bug_count, spawn_max):
            team2_nest.spawn_bug()
